/*
** SDLDisplay.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:06:35 2015 Alexis Bertholom
// Last update Tue Nov 21 19:59:26 2017 Lucas
*/

#ifndef SDLDISPLAY_HPP_
#define SDLDISPLAY_HPP_

#include <SDL2/SDL.h>
#include <string>
#include "Display.hpp"
#include "Image.hpp"
#include "SDLContext.hpp"

class SDLDisplay : protected SDLContext, public Display {
 public:
  SDLDisplay(std::string const& winTitle, Uint winW, Uint winH,
             Uint winX = SDL_WINDOWPOS_UNDEFINED,
             Uint winY = SDL_WINDOWPOS_UNDEFINED);
  ~SDLDisplay();

 public:
  void putPixel(Uint x, Uint y, Color color);
  void putRect(Uint x, Uint y, Uint w, Uint h, Color color);
  void putImage(Uint x, Uint y, Image& img, Uint w = Image::max,
                Uint h = Image::max);
  void putText(std::string const& msg, Uint x, Uint y, Color color);
  void putImage(std::string const& path, Uint x, Uint y);
  void clearScreen();
  void fillScreen(Color color);
  void putGrid(int tab[8][8], int x, int y);
  void refreshScreen();
  Uint getWinW() const;
  Uint getWinH() const;

 private:
  Uint _winW;
  Uint _winH;
  SDL_Window* _window;
  Image* _screen;
};

#endif
