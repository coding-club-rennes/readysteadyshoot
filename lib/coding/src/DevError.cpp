//
// DevError.cpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 23:38:03 2014 bertho_d
// Last update Sun Aug  3 23:46:14 2014 bertho_d
//

#include "DevError.hpp"

const char	*DevError::devErrorMessages[] = {
  "SDL context not initialized"
};

DevError::DevError(t_devErrCode errcode) : Error(DevError::devErrorMessages[errcode]), _errcode(errcode)
{
}
