/*
** Input.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub/context
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:08:55 2015 Alexis Bertholom
// Last update Tue Nov 21 20:02:05 2017 Lucas
*/
#include <cstring>
#include <SDL2/SDL.h>
#include "Input.hpp"

Input::Input()
{
  memset(this, 0, sizeof(Input));
  this->_pressed = -1;
}

Input::~Input()
{
}

/*
**public:
*/

bool		Input::getKeyState(SDL_Scancode key) const
{
  return (this->_keysState[key]);
}

bool		Input::getKeyStateOnce(SDL_Scancode key)
{
  if (!this->_keysState[key] || this->_alreadyPressed[key])
    return false;
  this->_alreadyPressed[key] = true;
  return (this->_keysState[key]);
}

bool		Input::getMouseButtonState(t_mouseButton button) const
{
  return (this->_mouseButtonsState[button]);
}

int		Input::getKeyPressed() const
{
  return (this->_pressed);
}

bool		Input::shouldExit() const
{
  return (this->_exit);
}

const int	*Input::getMousePos() const
{
  return (this->_mousePos);
}

const int	*Input::getMouseRel() const
{
  return (this->_mouseRel);
}

void		Input::flushEvents()
{
  SDL_Event	event;

  this->_mouseRel[0] = 0;
  this->_mouseRel[1] = 0;
  while (SDL_PollEvent(&event))
    {
      if (event.type == SDL_MOUSEMOTION)
	this->setMouse(event.motion);
      else if (event.type == SDL_KEYDOWN) {
	      this->_keysState[event.key.keysym.scancode] = true;
        this->_pressed = event.key.keysym.scancode;
      }
      else if (event.type == SDL_KEYUP) {
	      this->_alreadyPressed[event.key.keysym.scancode] = false;
	      this->_keysState[event.key.keysym.scancode] = false;
        this->_pressed = -1;
      }
      else if (event.type == SDL_MOUSEBUTTONDOWN)
	this->_mouseButtonsState[event.button.button] = true;
      else if (event.type == SDL_MOUSEBUTTONUP)
	this->_mouseButtonsState[event.button.button] = false;
      else if (event.type == SDL_WINDOWEVENT &&
	       event.window.event == SDL_WINDOWEVENT_CLOSE)
	this->_exit = true;
    }
}

/*
**private:
*/

void		Input::setMouse(SDL_MouseMotionEvent &motion)
{
  this->_mousePos[0] = motion.x;
  this->_mousePos[1] = motion.y;
  this->_mouseRel[0] = motion.xrel;
  this->_mouseRel[1] = motion.yrel;
}
