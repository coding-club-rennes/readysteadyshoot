//
// RNG.cpp for rng in /home/baillel/Documents/coding/2048
// 
// Made by baille_l
// Login   <baillel@epitech.net>
// 
// Started on  Mon Feb 22 22:57:20 2016 baille_l
// Last update Mon Feb 22 22:59:20 2016 baille_l
//

#include <stdlib.h>
#include <time.h>
#include "RNG.hpp"

int RNG::generate(int max)
{
  if (RNG::_initialized == false)
    {
      srand(time(NULL));
      RNG::_initialized = true;
    }
  return rand() % max;
}
